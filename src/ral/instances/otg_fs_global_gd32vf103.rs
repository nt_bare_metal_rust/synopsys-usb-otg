#![allow(non_snake_case, non_upper_case_globals)]
#![allow(non_camel_case_types)]
//! USB on the go full speed
//!
//! Used by: gd32vf103

#[cfg(not(feature = "nosync"))]
pub use super::super::peripherals::otg_fs_global_v1::Instance;
pub use super::super::peripherals::otg_fs_global_v1::{RegisterBlock, ResetValues};
pub use super::super::peripherals::otg_fs_global_v1::{
    CID, DIEPTXF0, DIEPTXF1, DIEPTXF2, DIEPTXF3, GAHBCFG, GCCFG, GINTMSK, GINTSTS, GNPTXSTS,
    GOTGCTL, GOTGINT, GRSTCTL, GRXFSIZ, GRXSTSP, GRXSTSR, GUSBCFG, HPTXFSIZ,
};

/// Access functions for the OTG_FS_GLOBAL peripheral instance
pub mod OTG_FS_GLOBAL {
    use super::ResetValues;

    #[cfg(not(feature = "nosync"))]
    use super::Instance;

    #[cfg(not(feature = "nosync"))]
    const INSTANCE: Instance = Instance {
        addr: 0x5000_0000,
        _marker: ::core::marker::PhantomData,
    };

    /// Reset values for each field in OTG_FS_GLOBAL
    pub const reset: ResetValues = ResetValues {
        GOTGCTL:  0x0000_0800,
        GOTGINT:  0x0000_0000,
        GAHBCFG:  0x0000_0000,
        GUSBCFG:  0x0000_0A80,
        GRSTCTL:  0x8000_0000,
        GINTSTS:  0x0400_0021,
        GINTMSK:  0x0000_0000,
        GRXSTSR:  0x0000_0000,
        GRXSTSP:  0x0000_0000,
        GRXFSIZ:  0x0000_0200,
        DIEPTXF0: 0x0200_0200,
        GNPTXSTS: 0x0008_0200,
        GCCFG:    0x0000_0000,
        CID:      0x0000_1000,
        HPTXFSIZ: 0x0200_0600,
        DIEPTXF1: 0x0200_0400,
        DIEPTXF2: 0x0200_0400,
        DIEPTXF3: 0x0200_0400,
    };
}

/// Raw pointer to OTG_FS_GLOBAL
///
/// Dereferencing this is unsafe because you are not ensured unique
/// access to the peripheral, so you may encounter data races with
/// other users of this peripheral. It is up to you to ensure you
/// will not cause data races.
///
/// This constant is provided for ease of use in unsafe code: you can
/// simply call for example `write_reg!(gpio, GPIOA, ODR, 1);`.
pub const OTG_FS_GLOBAL: *const RegisterBlock = 0x5000_0000 as *const _;
