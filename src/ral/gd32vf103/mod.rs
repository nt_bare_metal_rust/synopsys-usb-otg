#[cfg(all(feature = "gd32v", feature = "hs"))]
compile_error!("GD32VF103 driver supports only USBFS");

pub use super::instances::otg_fs_device_gd32vf103 as otg_fs_device;
pub use super::instances::otg_fs_global_gd32vf103 as otg_fs_global;
pub use super::instances::otg_fs_host;
//pub use super::instances::otg_hs_device;
//pub use super::instances::otg_hs_global;
//pub use super::instances::otg_hs_host;
pub use super::instances::otg_s_pwrclk;
